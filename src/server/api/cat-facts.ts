export default defineEventHandler(async () => {
  try {
    // Gitlab Pages does not supports Server-Side Rendered applications
    const response = await fetch("https://meowfacts.herokuapp.com/?count=10")
    const parsedResponse = await response.json()
    return parsedResponse.data
  } catch (error) {
    return { error: "Failed to fetch cat facts" }
  }
})