import { defineStore } from "pinia"
import { ref } from "vue"

export const useStore = defineStore("catFacts", () => {
  const catFact = ref<string>("")
  const isLoading = ref<boolean>(false)

  const fetchCatFacts = async () => {
    isLoading.value = true
    try {
      const cachedCatFacts = localStorage.getItem("catFacts")

      if (cachedCatFacts) {
        const parsedCatFacts: string[] = JSON.parse(cachedCatFacts)

        if (parsedCatFacts.length > 0) {
          const [newCatFact] = parsedCatFacts.splice(0, 1)
          catFact.value = newCatFact
          const stringifiedCatFacts = JSON.stringify(parsedCatFacts)
          localStorage.setItem("catFacts", stringifiedCatFacts)
          isLoading.value = false
          return
        }
      }

      // Gitlab Pages does not supports Server-Side Rendered applications
      // const response = await fetch("/api/cat-facts")
      const response = await fetch("https://meowfacts.herokuapp.com/?count=10")
      const { data: newCatFacts } = await response.json()

      if (newCatFacts && newCatFacts.length > 0) {
        const [newCatFact] = newCatFacts.splice(0, 1)
        catFact.value = newCatFact
        const stringifiedCatFacts = JSON.stringify(newCatFacts)
        localStorage.setItem("catFacts", stringifiedCatFacts)
      } else {
        console.error("No cat facts received from the API.")
      }
    } catch (error) {
      console.error("Failed to fetch cat facts:", error)
    } finally {
      isLoading.value = false
    }
  }

  return { catFact, isLoading, fetchCatFacts }
})
