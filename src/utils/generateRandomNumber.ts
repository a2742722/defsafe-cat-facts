export const generateRandomNumber = (minimum: number, maximum: number, options = { shouldRound: false }) => {
  const roundedMinimum = Math.ceil(minimum)
  const roundedMaximum = Math.floor(maximum)
  const randomNumber = Math.random() * (roundedMaximum - roundedMinimum + 1) + minimum
  return options.shouldRound ? Math.floor(randomNumber) : randomNumber
}