import type { NuxtConfig } from "nuxt/config"
import type { ModuleOptions } from "@nuxtjs/google-fonts"

const config: NuxtConfig & { googleFonts: ModuleOptions } = {
  ssr: false,
  srcDir: "./src",
  app: {
    head: {
      title: "Defsafe Cat Facts",
      link: [
        { rel: "icon", type: "image/png", href: "/favicon.png" }
      ]
    }
  },
  css: ["@/public/styles/global.css"],
  devtools: { enabled: true },
  modules: [
    "@nuxtjs/tailwindcss",
    "@nuxtjs/google-fonts",
    "@pinia/nuxt",
    "@nuxt/test-utils/module",
    "nuxt-mdi"
  ],
  googleFonts: {
    families: {
      Jost: "400..700"
    }
  }
}

export default defineNuxtConfig(config)