# Defsafe Cat Facts

A Single Page Application (SPA) to show random cat facts using Nuxt.js and Tailwind CSS.

## Table of Contents

- [Design Choices](#design-choices)
- [Features](#features)
- [Setup and Run](#setup-and-run)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Running the Project](#running-the-project)
  - [Running Tests](#running-tests)
- [API Documentation](#api-documentation)
- [Credits](#credits)

## Design Choices

The project makes use of a simple yet intuitive architecture, where files are divided into directories named after their category, no libraries (aside from those requested in the challenge requirements/bonuses) were used in order to keep the bundle size as minimal as possible.

Due to some unclear restrictions imposed by GitHub around GitHub Pages, the fact my repository was behaving like a private one even though i explicitly set it's visibility to public, and on top of that, not being able to create a Vercel account due to SMS verification issues, I had to resort to GitLab Pages, both repositories are identical and kept in synch through a `git pushall` alias, my global Git configuration currently looks like this:

```ini
[alias]
  pushall = "!git remote | xargs -L1 git push"
```

The GitLab repository can be found [here](https://gitlab.com/a2742722/defsafe-cat-facts)

Another important detail that is worth mentioning is that as GitLab Pages supports exclusively static pages, making API requests from the server side was not possible in practice, I kept the code for that in the codebase at `src/server/api/cat-facts.ts` alongside some code comments throughout the project, it was tested locally and works just fine.

## Features

- [x] Show random cat facts.
- [x] Responsive design for both small and large screens.
- [x] Cat facts caching through `localStorage`.
- [x] Server-side API requests.
- [x] Global state management through Pinia.
- [x] Tailwind CSS for styling.
- [x] Fade out/in animations.
- [x] Automated Tests (Unit Tests with Jest).

## Setup and Run

### Prerequisites

- Node.js (version 14.x or later)
- pnpm (version 6.x or later)

### Installation

1. Clone the repository:

  ```sh
  git clone https://gitlab.com/a2742722/defsafe-cat-facts.git
  cd defsafe-cat-facts
  ```

2. Install dependencies:

    ```sh
    pnpm install
    ```

### Running the Project

1. Start the development server:

  ```sh
  pnpm dev
  ```

2. Open your browser and navigate to:

  ```
  http://localhost:3000
  ```

### Running Tests

#### Unit Tests

Run unit tests with Jest:

  ```sh
  pnpm test
  ```

## API Documentation

This project uses the meowfacts API to fetch cat facts data.

- [mewofacts API Documentation](https://github.com/wh-iterabb-it/meowfacts)

## Credits

The Cat favicon used on this project was downloaded from [FlatIcon](https://www.flaticon.com/free-icon/cat_235349)