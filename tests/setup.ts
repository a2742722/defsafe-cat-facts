import { beforeAll, afterAll, vi } from "vitest"

beforeAll(() => {
  vi.spyOn(console, "error").mockImplementation(() => {})
  vi.spyOn(console, "warn").mockImplementation(() => {})
})

afterAll(() => {
  vi.restoreAllMocks()
})
