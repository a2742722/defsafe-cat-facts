import { describe, it, expect, vi, beforeEach } from "vitest"
import { mount } from "@vue/test-utils"
import KittyPictures from "../../src/components/KittyPictures.vue"
import { setActivePinia, createPinia } from "pinia"

describe("KittyPictures", () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it("renders the component correctly", () => {
    const wrapper = mount(KittyPictures)
    expect(wrapper.exists()).toBe(true)
  })

  it("contains exactly three kitty images", () => {
    const wrapper = mount(KittyPictures)
    expect(wrapper.findAll("img").length).toBe(3)
  })
})
