import { describe, it, expect, vi, beforeEach } from "vitest"
import { mount } from "@vue/test-utils"
import CatFacts from "../../src/components/CatFacts.vue"
import { setActivePinia, createPinia } from "pinia"
import { useStore } from "@/store"

describe("CatFacts", () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it("renders the component correctly", () => {
    const wrapper = mount(CatFacts)
    expect(wrapper.exists()).toBe(true)
  })

  it("computes randomLines correctly", () => {
    const wrapper = mount(CatFacts)
    const randomLines: { width: string }[] = wrapper.vm.randomLines
    expect([2, 3, 4, 5]).toContain(randomLines.length)
    randomLines.forEach(line => {
      expect(line.width).toMatch(/^[\d.]+%$/)
    })
  })

  it("fetches cat facts on button click", async () => {
    const wrapper = mount(CatFacts)
    const store = useStore()
    store.fetchCatFacts = vi.fn()
    const button = wrapper.find("button")
    await button.trigger("click")
    expect(store.fetchCatFacts).toHaveBeenCalled()
  })

  it("displays loading skeleton when loading", () => {
    const store = useStore()
    store.isLoading = true
    const wrapper = mount(CatFacts)
    expect(wrapper.findAll(".animate-pulse").length).toBeGreaterThan(0)
  })

  it("displays a cat fact when not loading", () => {
    const store = useStore()
    store.catFact = "A random cat fact"
    store.isLoading = false
    const wrapper = mount(CatFacts)
    expect(wrapper.find("p").text().length).toBeGreaterThan(0)
  })
})
