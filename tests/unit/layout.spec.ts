import { describe, it, expect, beforeEach } from "vitest"
import { mount } from "@vue/test-utils"
import Layout from "../../src/layouts/default.vue"
import { setActivePinia, createPinia } from "pinia"

describe("Layout", () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it("renders the component correctly", () => {
    const wrapper = mount(Layout)
    expect(wrapper.exists()).toBe(true)
  })

  it("displays the title", () => {
    const wrapper = mount(Layout)
    expect(wrapper.find("h1").text()).toBe("THE DEFSAFE CAT FACTS PAGE")
  })
})
